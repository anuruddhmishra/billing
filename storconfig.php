<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>Jangli maina(Retails Software for billing and Inventory)</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="css/css.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/online/jquery.min.js"></script>
    <script type="text/javascript" src="js/online/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>

</head>

<body>

<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><img src="images/logo13.png" alt="jagli mainna"></a>
        <a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
            <span class="sr-only">Toggle navbar</span>
            <i class="icon-grid3"></i>
        </button>
        <button type="button" class="navbar-toggle offcanvas">
            <span class="sr-only">Toggle navigation</span>
            <i class="icon-paragraph-justify2"></i>
        </button>
    </div>

    <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon-people"></i>
                <span class="label label-default">2</span>
            </a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                    <span>Activity</span>
                    <a href="#" class="pull-right"><i class="icon-paragraph-justify"></i></a>
                </div>
                <ul class="activity">
                    <li>
                        <i class="icon-cart-checkout text-success"></i>
                        <div>
                            <a href="#">Eugene</a> ordered 2 copies of <a href="#">OEM license</a>
                            <span>14 minutes ago</span>
                        </div>
                    </li>
                    <li>
                        <i class="icon-heart text-danger"></i>
                        <div>
                            Your <a href="#">latest post</a> was liked by <a href="#">Audrey Mall</a>
                            <span>35 minutes ago</span>
                        </div>
                    </li>
                    <li>
                        <i class="icon-checkmark3 text-success"></i>
                        <div>
                            Mail server was updated. See <a href="#">changelog</a>
                            <span>About 2 hours ago</span>
                        </div>
                    </li>
                    <li>
                        <i class="icon-paragraph-justify2 text-warning"></i>
                        <div>
                            There are <a href="#">6 new tasks</a> waiting for you. Don't forget!
                            <span>About 3 hours ago</span>
                        </div>
                    </li>
                </ul>
            </div>
        </li>

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon-paragraph-justify2"></i>
                <span class="label label-default">6</span>
            </a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                    <span>Messages</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <ul class="popup-messages">
                    <li class="unread">
                        <a href="#">
                            <img src="http://placehold.it/300" alt="" class="user-face">
                            <strong>Eugene Kopyov <i class="icon-attachment2"></i></strong>
                            <span>Aliquam interdum convallis massa...</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="http://placehold.it/300" alt="" class="user-face">
                            <strong>Jason Goldsmith <i class="icon-attachment2"></i></strong>
                            <span>Aliquam interdum convallis massa...</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="http://placehold.it/300" alt="" class="user-face">
                            <strong>Angel Novator</strong>
                            <span>Aliquam interdum convallis massa...</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="http://placehold.it/300" alt="" class="user-face">
                            <strong>Monica Bloomberg</strong>
                            <span>Aliquam interdum convallis massa...</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="http://placehold.it/300" alt="" class="user-face">
                            <strong>Patrick Winsleur</strong>
                            <span>Aliquam interdum convallis massa...</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle">
                <i class="icon-grid"></i>
            </a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                    <span>Tasks list</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Category</th>
                        <th class="text-center">Priority</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><span class="status status-success item-before"></span> <a href="#">Frontpage fixes</a></td>
                        <td><span class="text-smaller text-semibold">Bugs</span></td>
                        <td class="text-center"><span class="label label-success">87%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-danger item-before"></span> <a href="#">CSS compilation</a></td>
                        <td><span class="text-smaller text-semibold">Bugs</span></td>
                        <td class="text-center"><span class="label label-danger">18%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-info item-before"></span> <a href="#">Responsive layout changes</a></td>
                        <td><span class="text-smaller text-semibold">Layout</span></td>
                        <td class="text-center"><span class="label label-info">52%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-success item-before"></span> <a href="#">Add categories filter</a></td>
                        <td><span class="text-smaller text-semibold">Content</span></td>
                        <td class="text-center"><span class="label label-success">100%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-success item-before"></span> <a href="#">Media grid padding issue</a></td>
                        <td><span class="text-smaller text-semibold">Bugs</span></td>
                        <td class="text-center"><span class="label label-success">100%</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </li>

        <li class="user dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">
                <img src="http://placehold.it/300">
                <span>Admin</span>
                <i class="caret"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right icons-right">
                <li><a href="#"><i class="icon-user"></i> Profile</a></li>
                <li><a href="#"><i class="icon-bubble4"></i> Messages</a></li>
                <li><a href="#"><i class="icon-cog"></i> Settings</a></li>
                <li><a href="#"><i class="icon-exit"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- /navbar -->


<!-- Page container -->
<div class="page-container">


    <!-- Sidebar -->
    <div class="sidebar">
        <div class="sidebar-content">

            <!-- User dropdown -->
            <!-- hide by anuruddh mishra
            <div class="user-menu dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="http://placehold.it/300">
                    <div class="user-info">
                        Weaver Birds <span>provide peace of mind</span>
                    </div>
                </a>
                <div class="popup dropdown-menu dropdown-menu-right">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img src="http://placehold.it/300">
                            <div class="thumb-options">
                                <span>
                                    <a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a>
                                    <a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a>
                                </span>
                            </div>
                        </div>

                        <div class="caption text-center">
                            <h6>Madison Gartner <small>Front end developer</small></h6>
                        </div>
                    </div>

                    <ul class="list-group">
                        <li class="list-group-item"><i class="icon-pencil3 text-muted"></i> My posts <span class="label label-success">289</span></li>
                        <li class="list-group-item"><i class="icon-people text-muted"></i> Users online <span class="label label-danger">892</span></li>
                        <li class="list-group-item"><i class="icon-stats2 text-muted"></i> Reports <span class="label label-primary">92</span></li>
                        <li class="list-group-item"><i class="icon-stack text-muted"></i> Balance <h5 class="pull-right text-danger">$45.389</h5></li>
                    </ul>
                </div>
            </div>  anu -->
            <!-- /user dropdown -->


            <!-- Main navigation -->
            <ul class="navigation">
                <li class="active"><a href="index.html"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>
                <li>
                    <a href="#"><span>Items</span> <i class="icon-paragraph-justify2"></i></a>
                    <ul>
                        <li><a href="additem.html">Add Item</a></li>
                        <li><a href="upload.html">Import Item by cvs</a></li>
                        <li><a href="#">Item List</a></li>
                        <!--<li><a href="wysiwyg.html">WYSIWYG editor</a></li>
                        <li><a href="validation.html">Validation</a></li>
                        <li><a href="file_uploader.html">Multiple file uploader</a></li>
                        <li><a href="form_snippets.html">Form snippets</a></li>-->
                    </ul>
                </li>
                <li>
                    <a href="invoice.html"><span>Sales</span> <i class="icon-cart2"></i></a>
                    <!--<ul>
                        <li><a href="visuals.html">Visuals &amp; notifications</a></li>
                        <li><a href="navs.html">Navs</a></li>
                        <li><a href="panel_options.html">Panel options</a></li>
                        <li><a href="navbars.html">Navbars</a></li>
                        <li><a href="info_blocks.html">Info blocks</a></li>
                        <li><a href="icons.html">Icons <span class="label label-danger">850+</span></a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="calendar.html">Calendar</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="gallery.html">Media gallery</a></li>
                        <li><a href="header_elements.html">Page header elements</a></li>
                        <li><a href="content_grid.html">Content grid</a></li>
                    </ul>-->
                </li>
                <li>
                    <a href="#"><span>Customers</span> <i class="icon-users2"></i></a>
                    <ul>
                        <li><a href="addcustomer.html">Add Customer</a></li>
                        <li><a href="customerlist.php">Customer List</a></li>
                        <!--<li><a href="sidebar_narrow_left.html">Left narrow sidebar</a></li>
                        <li><a href="sidebar_narrow_right.html">Right narrow sidebar</a></li>
                        <li><a href="sidebar_icons_left.html">Left aligned icons</a></li>
                        <li><a href="layout_boxed.html">Boxed layout <span class="label label-danger">New</span></a></li>
                        <li><a href="layout_fixed_navbar.html">Fixed navbar</a></li>
                        <li><a href="horizontal_navigation.html">Horizontal navigation</a></li>
                        <li><a href="horizontal_sidebar.html">Sidebar &amp; Horizontal navigation</a></li>
                        <li><a href="navigation_disabled_items.html">Disabled navigation items</a></li> -->

                    </ul>
                </li>
                <!--<li>
                    <a href="#"><span>Task Manager</span> <i class="icon-numbered-list"></i></a>
                    <ul>
                        <li><a href="task_grid.html">Task grid</a></li>
                        <li><a href="task_list.html">Task list</a></li>
                        <li><a href="task_detailed.html">Task detailed</a></li>
                    </ul>
                </li>-->
                <li>
                    <a href="#"><span>Reports</span> <i class="icon-bars"></i></a>
                    <ul>
                        <li><a href="charts.html">Complete Report</a></li>
                        <li><a href="#">Sale Report</a></li>
                        <li><a href="#">Daily Sale</a></li>
                        <li><a href="#">Monthly Report</a></li>
                    </ul>>
                </li>
                <li>
                    <a href="#"><span>User</span> <i class="icon-user"></i></a>
                    <ul>
                        <li><a href="#">Add New User</a></li>
                        <li><a href="#">User List</a></li>
                        <li><a href="team.html">Team</a></li>
                        <li><a href="contacts.html">Contact list</a></li>

                    </ul>
                </li>
                <li>
                    <a href="#"><span>Setting</span> <i class="icon-cog"></i></a>
                    <ul>
                        <li><a href="#">Add Company Setting</a></li>
                        <li><a href="#">Tax Rate Setting</a></li>
                        <li><a href="#">System Setting</a></li>
                    </ul>
                </li>
                <!--<li>
                    <a href="#"><span>Tables</span> <i class="icon-table2"></i></a>
                    <ul>
                        <li><a href="tables_static.html">Static tables</a></li>
                        <li><a href="tables_dynamic.html">Data tables</a></li>
                    </ul>
                </li>
                <li><a href="search.html"><span>Search page</span> <i class="icon-search3"></i></a></li>
                <li>
                    <a href="#"><span>Invoice</span> <i class="icon-coin"></i></a>
                    <ul>
                        <li><a href="invoice.html">Invoice template</a></li>
                        <li><a href="invoice_list.html">Invoice list</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Login page</span> <i class="icon-user-plus"></i></a>
                    <ul>
                        <li><a href="login_simple.html">Simple login form</a></li>
                        <li><a href="login_advanced.html">Login form with user image</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Error pages</span> <i class="icon-warning"></i></a>
                    <ul>
                        <li><a href="403.html">403 page</a></li>
                        <li><a href="404.html">404 page</a></li>
                        <li><a href="405.html">405 page</a></li>
                        <li><a href="500.html">500 page</a></li>
                        <li><a href="503.html">503 page</a></li>
                        <li><a href="offline.html">Offline page</a></li>
                        <li><a href="error_sidebar.html">Error page with sidebar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Blank pages</span> <i class="icon-insert-template"></i></a>
                    <ul>
                        <li><a href="blank_left_sidebar.html">Left sidebar</a></li>
                        <li><a href="blank_right_sidebar.html">Right sidebar</a></li>
                        <li><a href="blank_collapsed_sidebar.html">Collapsed sidebar</a></li>
                        <li><a href="blank_full_width.html">Full width page</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Navigation levels</span> <i class="icon-stack"></i></a>
                    <ul>
                        <li><a href="#">Second level first item</a></li>
                        <li><a href="#">Second level second item</a>
                            <ul>
                                <li><a href="#">Third level first item</a></li>
                                <li><a href="#">Third level third item</a>
                                    <ul>
                                        <li><a href="#">Fourth level first item</a></li>
                                        <li><a href="#">Fourth level second item</a></li>
                                        <li><a href="#">Fourth level third item</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Third level second item</a></li>
                                <li><a href="#">Third level third item</a>
                                    <ul>
                                        <li><a href="#">Fourth level first item</a></li>
                                        <li><a href="#">Fourth level second item</a></li>
                                        <li><a href="#">Fourth level third item</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Third level second item</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Second level third item</a></li>
                    </ul>
                </li>-->
            </ul>
            <!-- /main navigation -->

        </div>
    </div>
    <!-- /sidebar -->


    <!-- Page content -->
    <div class="page-content">


        <!-- Page header -->
        <form class="form-horizontal" role="form" action="#" >

            <!-- Basic inputs -->
            <div class="panel panel-default" style="margin-top: 10px;">
                <div class="panel-heading"><h6 class="panel-title"><i class="icon-cogs"></i>Store Configration </h6></div>
                <div class="panel-body">
                    <div class="container">
                        <!-- Text input-->
                        <div class="panel-body">




                            <div class="main-content">


                                <div class="config-panel">
                                    <div class="row">
                                        <form action="" id="config_form" class="form-horizontal" autocomplete="off" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                                            <div class="col-md-12">
                                                <div class="panel panel-piluku">


                                                    <fieldset>

                                                        <legend><lable>

                                                            <div class="panel-heading">
                                                        Company Information </div>

                                                                </leble></legend>

                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label for="company_logo" class="col-sm-3 col-md-3  control-label ">Company Logo :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="file" name="company_logo" id="company_logo" class="filestyle" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="delete_logo" class="col-sm-3 col-md-3  control-label ">Delete Logo :</label> <div class="col-sm-9 col-md-9 ">
                                                                <input type="checkbox" name="delete_logo" value="1" id="delete_logo"/>
                                                                <label for="delete_logo"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="company" class="col-sm-3 col-md-3  control-label  required">Company Name:</label> <div class="col-sm-9 col-md-9  input-field">
                                                                <input type="text" name="company" value="Point Of Sale" class="validate form-control form-inps" id="company"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="website" class="col-sm-3 col-md-3  control-label ">Website :</label> <div class="col-sm-9 col-md-9 input-field">
                                                                <input type="text" name="website" value="" class="form-control form-inps" id="website"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                        </fieldset>




                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="panel panel-piluku">


                                                    <fieldset>
                                                        <legend><lable>
                                                            <div class="panel-heading">
                                                        Taxes & Currency </div>
                                                                </leble></legend>





                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label for="prices_include_tax" class="col-sm-3 col-md-3 control-label ">Prices include Tax :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="prices_include_tax" value="prices_include_tax"  id="prices_include_tax"/>
                                                                <label for="prices_include_tax"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="charge_tax_on_recv" class="col-sm-3 col-md-3  control-label ">Charge tax on receivings :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="charge_tax_on_recv" value="charge_tax_on_recv" id="charge_tax_on_recv"/>
                                                                <label for="charge_tax_on_recv"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="default_tax_1_rate" class="col-sm-3 col-md-3  control-label ">Tax 1 Rate :</label> <div class="col-sm-4 col-md-4">
                                                                <input type="text" name="default_tax_1_name" value="IVA" class="form-control form-inps" placeholder="Tax Name" id="default_tax_1_name" size="10"/>
                                                            </div>
                                                            <div class="col-sm-4 col-md-4">
                                                                <input type="text" name="default_tax_1_rate" value="10" class="form-control form-inps-tax" placeholder="Tax Percent" id="default_tax_1_rate" size="4"/>
                                                                <div class="tax-percent-icon">%</div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="default_tax_1_rate" class="col-sm-3 col-md-3  control-label ">Tax 2 Rate :</label> <div class="col-sm-4 col-md-4">
                                                                <input type="text" name="default_tax_2_name" value="VAT1" class="form-control form-inps" placeholder="Tax Name" id="default_tax_2_name" size="10"/>
                                                            </div>
                                                            <div class="col-sm-4 col-md-4 ">
                                                                <input type="text" name="default_tax_2_rate" value="5" class="form-control form-inps-tax" placeholder="Tax Percent" id="default_tax_2_rate" size="4"/>
                                                                <div class="tax-percent-icon">%</div>
                                                                <div class="clear"></div>

                                                            </div>

                                                            <div class="col-md-12 more_taxes_container" style="display: block">
                                                                <div class="form-group">
                                                                    <label for="default_tax_3_rate" class="col-sm-3 col-md-3  control-label ">Tax 3 Rate :</label> <div class="col-sm-4 col-md-4">
                                                                        <input type="text" name="default_tax_3_name" value="VAT2" class="form-control form-inps" placeholder="Tax Name" id="default_tax_3_name" size="10"/>
                                                                    </div>
                                                                    <div class="col-sm-4 col-md-4">
                                                                        <input type="text" name="default_tax_3_rate" value="10" class="form-control form-inps-tax" placeholder="Tax Percent" id="default_tax_3_rate" size="4"/>
                                                                        <div class="tax-percent-icon">%</div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="default_tax_4_rate" class="col-sm-3 col-md-3 control-label ">Tax 4 Rate :</label> <div class="col-sm-4 col-md-4">
                                                                        <input type="text" name="default_tax_4_name" value="VAT3" class="form-control form-inps" placeholder="Tax Name" id="default_tax_4_name" size="10"/>
                                                                    </div>
                                                                    <div class="col-sm-4 col-md-4 ">
                                                                        <input type="text" name="default_tax_4_rate" value="15" class="form-control form-inps-tax" placeholder="Tax Percent" id="default_tax_4_rate" size="4"/>
                                                                        <div class="tax-percent-icon">%</div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="default_tax_5_rate" class="col-sm-3 col-md-3 control-label ">Tax 5 Rate :</label> <div class="col-sm-4 col-md-4">
                                                                        <input type="text" name="default_tax_5_name" value="VAT4" class="form-control form-inps" placeholder="Tax Name" id="default_tax_5_name" size="10"/>
                                                                    </div>
                                                                    <div class="col-sm-4 col-md-4 ">
                                                                        <input type="text" name="default_tax_5_rate" value="20" class="form-control form-inps-tax" placeholder="Tax Percent" id="default_tax_5_rate" size="4"/>
                                                                        <div class="tax-percent-icon">%</div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="barcode_price_include_tax" class="col-sm-3 col-md-3  control-label ">Include tax on barcode? :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="barcode_price_include_tax" value="barcode_price_include_tax" id="barcode_price_include_tax"/>
                                                                <label for="barcode_price_include_tax"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="currency_symbol" class="col-sm-3 col-md-3  control-label ">Currency Symbol :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="text" name="currency_symbol" value="$" class="form-control form-inps" id="currency_symbol"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="number_of_decimals" class="col-sm-3 col-md-3  control-label">Number of decimals :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="number_of_decimals" class="form-control" id="number_of_decimals">
                                                                    <option value="" selected="selected">Let system decide (Recommended)</option>
                                                                    <option value="0">0</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="thousands_separator" class="col-sm-3 col-md-3 control-label">Thousands Separator :</label> <div class="col-sm-9 col-md-9 input-field">
                                                                <input type="text" name="thousands_separator" value="," class="validate form-control form-inps" id="thousands_separator"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="decimal_point" class="col-sm-3 col-md-3 control-label">Decimal Point :</label> <div class="col-sm-9 col-md-9 input-field">
                                                                <input type="text" name="decimal_point" value="." class="validate form-control form-inps" id="decimal_point"/>
                                                            </div>
                                                        </div>

                                                      <!--Currency Denominations :-->


                                                    </div>


                                                        </fieldset>

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="panel panel-piluku">

                                                    <fieldset>
                                                        <legend><lable>
                                                    <div class="panel-heading">
                                                        Sales & Receipt </div>

                                                                </leble></legend>

                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label for="sale_prefix" class="col-sm-3 col-md-3  control-label  required">Sale ID Prefix :</label> <div class="col-sm-9 col-md-9 ">
                                                                <input type="text" name="sale_prefix" value="ID-" class="form-control form-inps" id="sale_prefix"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="override_receipt_title" class="col-sm-3 col-md-3 control-label">Override receipt title :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="text" name="override_receipt_title" value="title" class="form-control form-inps" id="override_receipt_title"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="id_to_show_on_sale_interface" class="col-sm-3 col-md-3 control-label  required">Item ID to Show on Sales Interface :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="id_to_show_on_sale_interface" class="form-control" id="id_to_show_on_sale_interface">
                                                                    <option value="number">UPC/EAN/ISBN</option>
                                                                    <option value="product_id">Product ID</option>
                                                                    <option value="id" selected="selected">Item Id</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="show_item_id_on_receipt" class="col-sm-3 col-md-3 control-label ">Show item id on receipt :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="show_item_id_on_receipt" value="show_item_id_on_receipt" id="show_item_id_on_receipt"/>
                                                                <label for="show_item_id_on_receipt"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="print_after_sale" class="col-sm-3 col-md-3 control-label ">Print receipt after sale :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="print_after_sale" value="print_after_sale"  id="print_after_sale"/>
                                                                <label for="print_after_sale"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="print_after_receiving" class="col-sm-3 col-md-3 control-label ">Print receipt after receiving :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="print_after_receiving" value="print_after_receiving" id="print_after_receiving"/>
                                                                <label for="print_after_receiving"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="auto_focus_on_item_after_sale_and_receiving" class="col-sm-3 col-md-3 control-label ">Auto Focus On Item Field When using Sales/Receivings Interfaces :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="auto_focus_on_item_after_sale_and_receiving" value="auto_focus_on_item_after_sale_and_receiving" id="auto_focus_on_item_after_sale_and_receiving"/>
                                                                <label for="auto_focus_on_item_after_sale_and_receiving"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="hide_signature" class="col-sm-3 col-md-3 control-label ">Hide Signature :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="hide_signature" value="hide_signature" id="hide_signature"/>
                                                                <label for="hide_signature"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="number_of_recent_sales" class="col-sm-3 col-md-3 control-label ">Number of recent sales by customer to show :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="number_of_recent_sales" class="form-control" id="number_of_recent_sales">
                                                                    <option value="10" selected="selected">10</option>
                                                                    <option value="20">20</option>
                                                                    <option value="50">50</option>
                                                                    <option value="100">100</option>
                                                                    <option value="200">200</option>
                                                                    <option value="500">500</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="remove_customer_contact_info_from_receipt" class="col-sm-3 col-md-3 control-label ">Remove customer contact info from receipt:</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="remove_customer_contact_info_from_receipt" value="remove_customer_contact_info_from_receipt" id="remove_customer_contact_info_from_receipt"/>
                                                                <label for="remove_customer_contact_info_from_receipt"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="hide_customer_recent_sales" class="col-sm-3 col-md-3 control-label ">Hide Recent Sales for Customer :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="hide_customer_recent_sales" value="hide_customer_recent_sales" id="hide_customer_recent_sales"/>
                                                                <label for="hide_customer_recent_sales"><span></span></label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="round_cash_on_sales" class="col-sm-3 col-md-3 control-label ">Round to nearest .05 on receipt :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="round_cash_on_sales" value="round_cash_on_sales" id="round_cash_on_sales"/>
                                                                <label for="round_cash_on_sales"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="automatically_email_receipt" class="col-sm-3 col-md-3 control-label ">Automatically Email receipt :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="automatically_email_receipt" value="automatically_email_receipt" id="automatically_email_receipt"/>
                                                                <label for="automatically_email_receipt"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="automatically_print_duplicate_receipt_for_cc_transactions" class="col-sm-3 col-md-3 control-label ">Automatically print duplicate receipt for credit card transactions :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="automatically_print_duplicate_receipt_for_cc_transactions" value="automatically_print_duplicate_receipt_for_cc_transactions" id="automatically_print_duplicate_receipt_for_cc_transactions"/>
                                                                <label for="automatically_print_duplicate_receipt_for_cc_transactions"><span></span></label>
                                                            </div>
                                                        </div>





                                                        <div class="form-group">
                                                            <label for="default_payment_type" class="col-sm-3 col-md-3 control-label ">Default Payment Type :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="default_payment_type" class="form-control" id="default_payment_type">
                                                                    <option value="Cash" selected="selected">Cash</option>
                                                                    <option value="Check">Check</option>
                                                                    <option value="Gift Card">Gift Card</option>
                                                                    <option value="Debit Card">Debit Card</option>
                                                                    <option value="Credit Card">Credit Card</option>
                                                                    <option value="Store Account">Store Account</option>
                                                                    <option value="store account">store account</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="receipt_text_size" class="col-sm-3 col-md-3 control-label ">Receipt text size :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="receipt_text_size" class="form-control" id="receipt_text_size">
                                                                    <option value="small" selected="selected">Small</option>
                                                                    <option value="medium">Medium</option>
                                                                    <option value="large">Large</option>
                                                                    <option value="extra_large">Extra large</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="default_sales_person" class="col-sm-3 col-md-3 control-label ">Default sales person :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="default_sales_person" class="form-control" id="default_sales_person">
                                                                    <option value="logged_in_employee" selected="selected">Logged in employee</option>
                                                                    <option value="not_set">Not set</option>
                                                                </select>
                                                            </div>
                                                        </div>



                                                            <div id="loyalty_setup_simple" style="display_none;">
                                                                <div class="form-group">
                                                                    <label for="number_of_sales_for_discount" class="col-sm-3 col-md-3 control-label ">Number of sales for discount :</label> <div class="col-sm-9 col-md-9">
                                                                        <input type="text" name="number_of_sales_for_discount" value="" class="validate form-control form-inps" id="number_of_sales_for_discount"/>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="discount_percent_earned" class="col-sm-3 col-md-3 control-label ">Discount percent earned when reaching sales :</label> <div class="col-sm-9 col-md-9">
                                                                        <input type="text" name="discount_percent_earned" value="0" class="validate form-control form-inps" id="discount_percent_earned"/>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="hide_sales_to_discount_on_receipt" class="col-sm-3 col-md-3 control-label ">Hide sales to discount on receipt :</label> <div class="col-sm-9 col-md-9">
                                                                        <input type="checkbox" name="hide_sales_to_discount_on_receipt" value="hide_sales_to_discount_on_receipt" id="hide_sales_to_discount_on_receipt"/>
                                                                        <label for="hide_sales_to_discount_on_receipt"><span></span></label>
                                                                    </div>
                                                                </div>
                                                            </div>






                                                        <div class="form-group">
                                                            <label for="edit_item_price_if_zero_after_adding" class="col-sm-3 col-md-3 control-label ">Edit item price if 0 after adding to sale :</label> <div class="col-sm-9 col-md-9">
                                                                <input type="checkbox" name="edit_item_price_if_zero_after_adding" value="edit_item_price_if_zero_after_adding" id="edit_item_price_if_zero_after_adding"/>
                                                                <label for="edit_item_price_if_zero_after_adding"><span></span></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="number_of_items_per_page" class="col-sm-3 col-md-3 control-label  required">Number Of Items Per Page :</label> <div class="col-sm-9 col-md-9 ">
                                                                <select name="number_of_items_per_page" class="form-control" id="number_of_items_per_page">
                                                                    <option value="20" selected="selected">20</option>
                                                                    <option value="50">50</option>
                                                                    <option value="100">100</option>
                                                                    <option value="200">200</option>
                                                                    <option value="500">500</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="number_of_items_in_grid" class="col-sm-3 col-md-3 control-label  required">Number of items per page in grid :</label> <div class="col-sm-9 col-md-9 ">
                                                                <select name="number_of_items_in_grid" class="form-control" id="number_of_items_in_grid">
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14" selected="">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="27">27</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                    <option value="31">31</option>
                                                                    <option value="32">32</option>
                                                                    <option value="33">33</option>
                                                                    <option value="34">34</option>
                                                                    <option value="35">35</option>
                                                                    <option value="36">36</option>
                                                                    <option value="37">37</option>
                                                                    <option value="38">38</option>
                                                                    <option value="39">39</option>
                                                                    <option value="40">40</option>
                                                                    <option value="41">41</option>
                                                                    <option value="42">42</option>
                                                                    <option value="43">43</option>
                                                                    <option value="44">44</option>
                                                                    <option value="45">45</option>
                                                                    <option value="46">46</option>
                                                                    <option value="47">47</option>
                                                                    <option value="48">48</option>
                                                                    <option value="49">49</option>
                                                                    <option value="50">50</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="return_policy" class="col-sm-3 col-md-3  control-label required">Return Policy :</label> <div class="col-sm-9 col-md-9">
                                                                <textarea name="return_policy" cols="30" rows="4" id="return_policy" class="form-control text-area">Change return policy</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="announcement_special" class="col-sm-3 col-md-3  control-label">Announcements/Specials :</label> <div class="col-sm-9 col-md-9">
                                                                <textarea name="announcement_special" cols="30" rows="4" id="announcement_special" class="form-control text-area"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="spreadsheet_format" class="col-sm-3 col-md-3 control-label ">Spreadsheet Format :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="spreadsheet_format" class="form-control" id="spreadsheet_format">
                                                                    <option value="CSV">CSV</option>
                                                                    <option value="XLSX" selected="selected">XLSX</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="mailing_labels_type" class="col-sm-3 col-md-3  control-label ">Mailing Labels Format :</label> <div class="col-sm-9 col-md-9 ">
                                                                <select name="mailing_labels_type" class="form-control" id="mailing_labels_type">
                                                                    <option value="pdf" selected="selected">PDF</option>
                                                                    <option value="excel">Excel</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="phppos_session_expiration" class="col-sm-3 col-md-3 control-label ">Session expiration :</label> <div class="col-sm-9 col-md-9">
                                                                <select name="phppos_session_expiration" class="form-control" id="phppos_session_expiration">
                                                                    <option value="0" selected="selected">On Browser Close</option>
                                                                    <option value="3600">1 Hours</option>
                                                                    <option value="7200">2 Hours</option>
                                                                    <option value="10800">3 Hours</option>
                                                                    <option value="14400">4 Hours</option>
                                                                    <option value="18000">5 Hours</option>
                                                                    <option value="21600">6 Hours</option>
                                                                    <option value="25200">7 Hours</option>
                                                                    <option value="28800">8 Hours</option>
                                                                    <option value="32400">9 Hours</option>
                                                                    <option value="36000">10 Hours</option>
                                                                    <option value="39600">11 Hours</option>
                                                                    <option value="43200">12 Hours</option>
                                                                    <option value="46800">13 Hours</option>
                                                                    <option value="50400">14 Hours</option>
                                                                    <option value="54000">15 Hours</option>
                                                                    <option value="57600">16 Hours</option>
                                                                    <option value="61200">17 Hours</option>
                                                                    <option value="64800">18 Hours</option>
                                                                    <option value="68400">19 Hours</option>
                                                                    <option value="72000">20 Hours</option>
                                                                    <option value="75600">21 Hours</option>
                                                                    <option value="79200">22 Hours</option>
                                                                    <option value="82800">23 Hours</option>
                                                                    <option value="86400">24 Hours</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <input type="submit" name="submitf" value="Submit" id="submitf" class="submit_button floating-button btn btn-primary btn-lg pull-right"/>
                                                        </div>
                                                    </div>


                                                        </fieldset>


                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>






                        </div>
                    </div>
                </div>
        </form>
        <!-- /form striped -->




</div>
 </div>   <!-- Footer -->
    <div class="footer clearfix">
        <div class="pull-left">&copy; 2016. <a href="http://themeforest.net/user/Kopyov">weaver bird</a></div>
        <div class="pull-right icons-group">
            <a href="#"><i class="icon-screen2"></i></a>
            <a href="#"><i class="icon-balance"></i></a>
            <a href="#"><i class="icon-cog3"></i></a>
        </div>
    </div>
    <!-- /footer -->



    <!-- /page content -->



<!-- /page container -->

</body>
</html>